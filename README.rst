==================
logging_component
==================

:Version: 0.1.0
:Source: https://bitbucket.org/maykinmedia/logging_component
:Keywords: ``<keywords>``
:PythonVersion: 3.10

|build-status| |requirements|

``<oneliner describing the project>``

Developed by `Maykin Media B.V.`_ for ``<client>``


Introduction
============

``<describe the project in a few paragraphs and briefly mention the features>``


Documentation
=============

See ``INSTALL.rst`` for installation instructions, available settings and
commands.


References
==========

* `Issues <https://taiga.maykinmedia.nl/project/logging_component>`_
* `Code <https://bitbucket.org/maykinmedia/logging_component>`_


.. |build-status| image:: http://jenkins.maykin.nl/buildStatus/icon?job=bitbucket/logging_component/master
    :alt: Build status
    :target: http://jenkins.maykin.nl/job/logging_component

.. |requirements| image:: https://requires.io/bitbucket/maykinmedia/logging_component/requirements.svg?branch=master
     :target: https://requires.io/bitbucket/maykinmedia/logging_component/requirements/?branch=master
     :alt: Requirements status


.. _Maykin Media B.V.: https://www.maykinmedia.nl
