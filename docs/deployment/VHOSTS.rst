Apache + mod-wsgi configuration
===============================

An example Apache2 vhost configuration follows::

    WSGIDaemonProcess logging_component-<target> threads=5 maximum-requests=1000 user=<user> group=staff
    WSGIRestrictStdout Off

    <VirtualHost *:80>
        ServerName my.domain.name

        ErrorLog "/srv/sites/logging_component/log/apache2/error.log"
        CustomLog "/srv/sites/logging_component/log/apache2/access.log" common

        WSGIProcessGroup logging_component-<target>

        Alias /media "/srv/sites/logging_component/media/"
        Alias /static "/srv/sites/logging_component/static/"

        WSGIScriptAlias / "/srv/sites/logging_component/src/logging_component/wsgi/wsgi_<target>.py"
    </VirtualHost>


Nginx + uwsgi + supervisor configuration
========================================

Supervisor/uwsgi:
-----------------

.. code::

    [program:uwsgi-logging_component-<target>]
    user = <user>
    command = /srv/sites/logging_component/env/bin/uwsgi --socket 127.0.0.1:8001 --wsgi-file /srv/sites/logging_component/src/logging_component/wsgi/wsgi_<target>.py
    home = /srv/sites/logging_component/env
    master = true
    processes = 8
    harakiri = 600
    autostart = true
    autorestart = true
    stderr_logfile = /srv/sites/logging_component/log/uwsgi_err.log
    stdout_logfile = /srv/sites/logging_component/log/uwsgi_out.log
    stopsignal = QUIT

Nginx
-----

.. code::

    upstream django_logging_component_<target> {
      ip_hash;
      server 127.0.0.1:8001;
    }

    server {
      listen :80;
      server_name  my.domain.name;

      access_log /srv/sites/logging_component/log/nginx-access.log;
      error_log /srv/sites/logging_component/log/nginx-error.log;

      location /500.html {
        root /srv/sites/logging_component/src/logging_component/templates/;
      }
      error_page 500 502 503 504 /500.html;

      location /static/ {
        alias /srv/sites/logging_component/static/;
        expires 30d;
      }

      location /media/ {
        alias /srv/sites/logging_component/media/;
        expires 30d;
      }

      location / {
        uwsgi_pass django_logging_component_<target>;
      }
    }
