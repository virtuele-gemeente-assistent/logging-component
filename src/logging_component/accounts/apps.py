from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = "logging_component.accounts"
