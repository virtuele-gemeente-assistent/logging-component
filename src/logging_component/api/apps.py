from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = "logging_component.api"
