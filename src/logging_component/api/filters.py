from django_filters import rest_framework as filters

from logging_component.events.models import Event
from logging_component.statistics.models import ConversationStatistics


class EventFilter(filters.FilterSet):
    session_id = filters.BaseInFilter()
    event_type = filters.BaseInFilter()
    agent_type = filters.BaseInFilter()
    organisation_id = filters.BaseInFilter()
    source = filters.BaseInFilter()
    channel = filters.BaseInFilter()

    exclude_messages = filters.BaseInFilter(field_name="message", exclude=True)
    timestamp__lte = filters.DateTimeFilter(field_name="timestamp", lookup_expr="lte")
    timestamp__lt = filters.DateTimeFilter(field_name="timestamp", lookup_expr="lt")
    timestamp__gte = filters.DateTimeFilter(field_name="timestamp", lookup_expr="gte")

    class Meta:
        model = Event
        fields = [
            "session_id",
            "event_type",
            "agent_type",
            "organisation_id",
            "source",
            "channel",
            "exclude_messages",
            "test_session",
            "timestamp__lte",
            "timestamp__lt",
            "timestamp__gte",
        ]


class ConversationStatisticsFilter(filters.FilterSet):
    upl = filters.CharFilter(lookup_expr="icontains")
    start__lte = filters.DateTimeFilter(field_name="start", lookup_expr="lte")
    start__lt = filters.DateTimeFilter(field_name="start", lookup_expr="lt")
    start__gte = filters.DateTimeFilter(field_name="start", lookup_expr="gte")
    end__lte = filters.DateTimeFilter(field_name="end", lookup_expr="lte")
    end__gte = filters.DateTimeFilter(field_name="end", lookup_expr="gte")

    class Meta:
        model = ConversationStatistics
        fields = [
            "start__lte",
            "start__lt",
            "start__gte",
            "end__lte",
            "end__gte",
            "organisation_id",
            "source",
            "escalate",
            "escalate_result",
            "feedback",
            "feedback_result",
            "upl",
        ]


class AggregatedStatisticsFilter(ConversationStatisticsFilter):
    group_by_x = filters.BaseCSVFilter()
    group_by_y = filters.BaseCSVFilter()

    class Meta(ConversationStatisticsFilter.Meta):
        model = ConversationStatistics
        fields = ConversationStatisticsFilter.Meta.fields + [
            "group_by_x",
            "group_by_y",
        ]
