from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from rest_framework import serializers

from logging_component.config.models import MunicipalityConfiguration
from logging_component.events.models import Event
from logging_component.statistics.models import ConversationStatistics


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = [
            "event_id",
            "session_id",
            "timestamp",
            "event_type",
            "agent_type",
            "sender_agent_id",
            "recipient_agent_id",
            "message",
            "buttons",
            "incoming_data",
            "outgoing_data",
            "translated_from",
            "translated_by",
            "translated_to",
            "organisation_id",
            "source",
            "channel",
            "test_session",
        ]
        extra_kwargs = {"event_id": {"read_only": True}}


class MunicipalityConfigurationSerializer(serializers.ModelSerializer):
    class Meta:
        model = MunicipalityConfiguration
        fields = [
            "municipality_slug",
            "archiving_period",
        ]

    def validate(self, attrs):
        valid_attrs = super().validate(attrs)

        if self.instance:
            if (
                "municipality_slug" in valid_attrs
                and valid_attrs["municipality_slug"] != self.instance.municipality_slug
            ):
                raise ValidationError(
                    {"municipality_slug": _("Cannot update `municipality_slug`")}
                )

        return valid_attrs


class ConversationStatisticsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConversationStatistics
        fields = [
            "session_id",
            "organisation_id",
            "start",
            "end",
            "source",
            "escalate",
            "escalate_result",
            "feedback",
            "feedback_result",
            "upl",
            "performance",
            "is_evaluated",
            "subjects",
            "num_correct_answers",
            "num_correct_answers_after_one_correction",
            "num_correct_answers_after_two_corrections",
            "num_failed_corrections",
            "num_incorrect_responses",
            "num_impossible_questions",
            "citizen_properly_assisted",
            "critical_error",
            "different_language",
            "personal_question",
            "remarks",
        ]


class AggregatedDatasetSerializer(serializers.Serializer):
    escalate = serializers.CharField(allow_blank=True, required=False)
    escalate_result = serializers.CharField(allow_blank=True, required=False)
    feedback = serializers.CharField(allow_blank=True, required=False)
    feedback_result = serializers.CharField(allow_blank=True, required=False)
    source = serializers.CharField(allow_blank=True, required=False)
    year = serializers.IntegerField(required=False)
    iso_weekdate_year = serializers.IntegerField(required=False)
    month = serializers.IntegerField(required=False)
    week = serializers.IntegerField(required=False)
    weekday = serializers.IntegerField(required=False)
    date = serializers.IntegerField(required=False)
    hour = serializers.IntegerField(required=False)

    data = serializers.ListField()
    performance = serializers.ListField()
    upl = serializers.ListField()


class AggregatedStatisticsSerializer(serializers.Serializer):
    labels = serializers.ListField()
    datasets = serializers.ListField(child=AggregatedDatasetSerializer())
