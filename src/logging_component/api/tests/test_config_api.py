from django.urls import reverse

from rest_framework.test import APITestCase

from logging_component.config.tests.factories import MunicipalityConfigurationFactory


class ConfigAPITestCase(APITestCase):
    def setUp(self):
        super().setUp()

        MunicipalityConfigurationFactory.create(
            municipality_slug="roosendaal", archiving_period=30
        )
        MunicipalityConfigurationFactory.create(
            municipality_slug="utrecht", archiving_period=15
        )
        MunicipalityConfigurationFactory.create(
            municipality_slug="rotterdam", archiving_period=365
        )

    def test_list_config(self):
        response = self.client.get(reverse("municipalityconfiguration-list"))

        expected = [
            {"municipality_slug": "roosendaal", "archiving_period": 30},
            {"municipality_slug": "utrecht", "archiving_period": 15},
            {"municipality_slug": "rotterdam", "archiving_period": 365},
        ]
        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.data, expected)

    def test_create_config(self):
        response = self.client.post(
            reverse("municipalityconfiguration-list"),
            {"municipality_slug": "amsterdam", "archiving_period": 12},
        )

        expected = {"municipality_slug": "amsterdam", "archiving_period": 12}

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data, expected)

    def test_create_config_default_archiving_period(self):
        response = self.client.post(
            reverse("municipalityconfiguration-list"),
            {"municipality_slug": "amsterdam"},
        )

        expected = {"municipality_slug": "amsterdam", "archiving_period": 30}

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data, expected)

    def test_read_config(self):
        response = self.client.get(
            reverse(
                "municipalityconfiguration-detail",
                kwargs={"municipality_slug": "roosendaal"},
            )
        )

        expected = {"municipality_slug": "roosendaal", "archiving_period": 30}

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, expected)

    def test_update_config(self):
        response = self.client.put(
            reverse(
                "municipalityconfiguration-detail",
                kwargs={"municipality_slug": "roosendaal"},
            ),
            {"municipality_slug": "roosendaal", "archiving_period": 15},
        )

        expected = {"municipality_slug": "roosendaal", "archiving_period": 15}

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, expected)

    def test_partial_update_config(self):
        response = self.client.patch(
            reverse(
                "municipalityconfiguration-detail",
                kwargs={"municipality_slug": "roosendaal"},
            ),
            {"archiving_period": 15},
        )

        expected = {"municipality_slug": "roosendaal", "archiving_period": 15}

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, expected)

    def test_partial_update_config_cannot_modify_slug(self):
        response = self.client.patch(
            reverse(
                "municipalityconfiguration-detail",
                kwargs={"municipality_slug": "roosendaal"},
            ),
            {"municipality_slug": "Roosendaal"},
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["municipality_slug"][0].code, "invalid")
