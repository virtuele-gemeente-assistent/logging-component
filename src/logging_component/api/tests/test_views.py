from uuid import uuid4

from django.urls import reverse

from rest_framework.test import APITestCase

from logging_component.events.tests.factories import EventFactory


class EventViewSetTestCase(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.dialog_uuid_1 = uuid4()
        cls.dialog_uuid_2 = uuid4()
        cls.dialog_uuid_3 = uuid4()

        EventFactory.create(
            message="/greet{hallo}",
            session_id=cls.dialog_uuid_1,
            timestamp="2020-02-02 18:00",
            event_type="user_message",
        )  # skipped
        EventFactory.create(
            message="Hallo, ik ben gem",
            session_id=cls.dialog_uuid_2,
            timestamp="2020-02-02 18:09",
            event_type="agent_message",
        )  # last

        EventFactory.create(
            message="Wat pasport",
            session_id=cls.dialog_uuid_2,
            timestamp="2020-02-02 18:10",
            event_type="user_message",
        )  # last
        EventFactory.create(
            message="/direct_escalatie",
            session_id=cls.dialog_uuid_3,
            timestamp="2020-02-02 18:20",
            event_type="user_message",
        )  # second
        EventFactory.create(
            message="some message",
            session_id=cls.dialog_uuid_1,
            timestamp="2020-02-02 18:30",
            event_type="user_message",
        )  # first
        EventFactory.create(
            message="How to change email?",
            session_id=cls.dialog_uuid_3,
            timestamp="2020-02-02 18:40",
            event_type="user_message",
        )  # not first event in session
        EventFactory.create(
            message="How to change two factor?",
            session_id=cls.dialog_uuid_3,
            timestamp="2020-02-02 18:50",
            event_type="user_message",
        )  # not first event in session

    def test_list_first(self):

        """
        Returns the first event non '/greet' event in a session and sorts in reverse chronological order.

        """
        url = reverse("event-list-first", args=[])

        response = self.client.get(url, format="json")
        data = response.json()

        self.assertEqual(data["count"], 3)
        self.assertEqual(data["total_pages"], 1)

        results = data["results"]
        self.assertEqual(len(results), 3)
        self.assertEqual(results[0]["message"], "some message")
        self.assertEqual(results[0]["session_id"], str(self.dialog_uuid_1))

        self.assertEqual(results[1]["message"], "/direct_escalatie")
        self.assertEqual(results[1]["session_id"], str(self.dialog_uuid_3))

        self.assertEqual(results[2]["message"], "Wat pasport")
        self.assertEqual(results[2]["session_id"], str(self.dialog_uuid_2))

    def test_list_first_sort(self):

        """
        Test default non default ordering
        """

        url = reverse("event-list-first", args=[])

        response = self.client.get(url, {"ordering": "timestamp"}, format="json")
        data = response.json()

        self.assertEqual(data["count"], 3)
        self.assertEqual(data["total_pages"], 1)

        results = data["results"]

        self.assertEqual(results[0]["message"], "Wat pasport")
        self.assertEqual(results[0]["session_id"], str(self.dialog_uuid_2))

        self.assertEqual(results[2]["message"], "some message")
        self.assertEqual(results[2]["session_id"], str(self.dialog_uuid_1))

    def test_list_first_exclude_messages(self):

        """
        Test events with certain message can be excluded
        Used to exclude /direct_escalatie events
        """

        url = reverse("event-list-first")

        response = self.client.get(
            url, {"exclude_messages": "/direct_escalatie"}, format="json"
        )
        data = response.json()

        self.assertEqual(data["count"], 2)
        self.assertEqual(data["total_pages"], 1)

        results = data["results"]
        self.assertEqual(len(results), 2)
        self.assertEqual(results[0]["message"], "some message")
        self.assertEqual(results[0]["session_id"], str(self.dialog_uuid_1))

        self.assertEqual(results[1]["message"], "Wat pasport")
        self.assertEqual(results[1]["session_id"], str(self.dialog_uuid_2))
