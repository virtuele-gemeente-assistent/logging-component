from django.urls import include, path

from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularRedocView,
    SpectacularSwaggerView,
)
from rest_framework import routers

from .views import (
    AggregatedStatisticsViewSet,
    ConversationStatisticsViewSet,
    EventViewSet,
    MunicipalityConfigurationViewSet,
)

router = routers.DefaultRouter()
router.register(r"events", EventViewSet)
router.register(r"config", MunicipalityConfigurationViewSet)
router.register(r"statistics", ConversationStatisticsViewSet)
router.register(r"aggregated-statistics", AggregatedStatisticsViewSet)

urlpatterns = [
    path("", include(router.urls)),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    # API documentation
    path("schema/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "schema/swagger-ui/",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="swagger-ui",
    ),
    path(
        "schema/redoc/", SpectacularRedocView.as_view(url_name="schema"), name="redoc"
    ),
]
