import logging
import math
from collections import Counter, OrderedDict

from django.db.models import Subquery

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import mixins, response, viewsets
from rest_framework.decorators import action
from rest_framework.filters import OrderingFilter
from rest_framework.response import Response

from logging_component.config.models import MunicipalityConfiguration
from logging_component.events.models import Event
from logging_component.statistics.models import ConversationStatistics

from .filters import (
    AggregatedStatisticsFilter,
    ConversationStatisticsFilter,
    EventFilter,
)
from .pagination import EventSetPagination
from .serializers import (
    AggregatedStatisticsSerializer,
    ConversationStatisticsSerializer,
    EventSerializer,
    MunicipalityConfigurationSerializer,
)

EVALUATION_COUNT_FIELDS = [
    "is_evaluated",
    "num_correct_answers",
    "num_correct_answers_after_one_correction",
    "num_correct_answers_after_two_corrections",
    "num_failed_corrections",
    "num_incorrect_responses",
    "num_impossible_questions",
]

EVALUATION_BOOLEAN_FIELDS = [
    "citizen_properly_assisted",  # TODO this field is a NullBoolean, will probably change in the future
    "critical_error",
    "different_language",
    "personal_question",
]

logger = logging.getLogger(__name__)


def custom_sort(item):
    if item[0] is False:
        return (0, item[1:], item[0])
    elif item[0] is True:
        return (1, item[1:], item[0])
    else:
        return (2, item[1:], item[0])


class EventViewSet(
    mixins.CreateModelMixin, mixins.DestroyModelMixin, viewsets.ReadOnlyModelViewSet
):
    queryset = Event.objects.order_by("-timestamp")
    serializer_class = EventSerializer
    lookup_field = "event_id"
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = EventFilter
    ordering_fields = ["timestamp"]

    pagination_class = EventSetPagination

    @action(
        methods=["get"],
        detail=False,
    )
    def list_first(self, request, *args, **kwargs):

        queryset = self.get_queryset()
        queryset = queryset.filter(
            id__in=Subquery(
                queryset.filter(event_type="user_message")
                .exclude(message__startswith="/greet")
                .order_by("session_id", "timestamp")
                .distinct("session_id")
                .values("id")
            )
        )
        queryset = self.filter_queryset(queryset)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class MunicipalityConfigurationViewSet(
    mixins.CreateModelMixin, mixins.UpdateModelMixin, viewsets.ReadOnlyModelViewSet
):
    queryset = MunicipalityConfiguration.objects.all()
    serializer_class = MunicipalityConfigurationSerializer
    lookup_field = "municipality_slug"


class ConversationStatisticsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = ConversationStatistics.objects.order_by("-start")
    serializer_class = ConversationStatisticsSerializer
    lookup_field = "session_id"
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = ConversationStatisticsFilter
    ordering_fields = ["start"]


class AggregatedStatisticsViewSet(viewsets.GenericViewSet):
    queryset = ConversationStatistics.objects.order_by("-start")
    serializer_class = AggregatedStatisticsSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = AggregatedStatisticsFilter
    ordering_fields = ["start"]

    # TODO clean this up and use serializer
    def list(self, request, *args, **kwargs):
        statistics = self.filter_queryset(self.get_queryset())

        group_by_x = tuple(request.query_params.get("groupByX", "single").split(","))
        group_by_y = tuple(request.query_params.get("groupByY", "single").split(","))

        if group_by_x and not group_by_y:
            group_by_y.append("single")
        if group_by_y and not group_by_x:
            group_by_x.append("single")

        tempdict = {}
        templist = []
        yvalues = []
        xvalues = []

        for conversation in statistics:
            xkeys = []
            ykeys = []
            for k in group_by_x:
                # FIX FOR GROUP ON UPL
                kv = getattr(conversation, k, "single")
                if type(kv) is list:
                    logger.info("list groupkey: {}".format(kv))
                    if len(kv):
                        kv = list(set(kv))
                        kv.sort()
                        kv = ",".join(kv)
                        logger.info("list groupkey unique: {}".format(kv))
                    else:
                        kv = ""
                xkeys.append(kv)
            for k in group_by_y:
                # FIX FOR GROUP ON UPL
                kv = getattr(conversation, k, "single")
                if type(kv) is list:
                    logger.info("list groupkey: {}".format(kv))
                    if len(kv):
                        kv = list(set(kv))
                        kv.sort()
                        kv = ",".join(kv)
                        logger.info("list groupkey unique: {}".format(kv))
                    else:
                        kv = ""
                ykeys.append(kv)
            tx = tuple(xkeys)
            ty = tuple(ykeys)
            if tx not in xvalues:
                xvalues.append(tx)
            if ty not in yvalues:
                yvalues.append(ty)
            ydict = tempdict.get(ty, {})
            tempdict[ty] = ydict
            gi = ydict.get(
                tx,
                {
                    **{
                        "performance": {},
                        "count": 0,
                        "upl": [],
                        "subjects": [],
                        "escalate_wait": {},
                    },
                    **{
                        field: 0
                        for field in EVALUATION_COUNT_FIELDS + EVALUATION_BOOLEAN_FIELDS
                    },
                },
            )
            for key, value in conversation.performance.items():
                gi["performance"][key] = gi["performance"].get(key, 0) + value
            wc = math.ceil((conversation.escalate_wait or 0) / 5) * 5
            if wc:
                gi["escalate_wait"][wc] = gi["escalate_wait"].get(wc, 0) + 1
            gi["upl"].extend(conversation.upl)
            gi["count"] = gi["count"] + 1
            if conversation.is_evaluated:
                for field in EVALUATION_COUNT_FIELDS:
                    gi[field] = gi[field] + (getattr(conversation, field) or 0)

                for field in EVALUATION_BOOLEAN_FIELDS:
                    gi[field] = gi[field] + int(getattr(conversation, field) or 0)

                gi["subjects"].extend(conversation.subjects)
            tempdict[ty][tx] = gi

        yvalues = sorted(yvalues, key=custom_sort)
        xvalues = sorted(xvalues)

        data = {}
        for k, v in tempdict.items():
            i = OrderedDict()
            i["data"] = []
            i["performance"] = []
            for field in EVALUATION_COUNT_FIELDS + EVALUATION_BOOLEAN_FIELDS:
                i[field] = []
            i["upl"] = []
            i["subjects"] = []
            i["escalate_wait"] = []
            for x in xvalues:
                d = v.get(
                    x,
                    {
                        **{
                            "performance": {},
                            "count": 0,
                            "upl": [],
                            "subjects": [],
                            "escalate_wait": {},
                            "num_correct_answers": 0,
                        },
                        **{
                            field: 0
                            for field in EVALUATION_COUNT_FIELDS
                            + EVALUATION_BOOLEAN_FIELDS
                        },
                    },
                )
                i["data"].append(d["count"])
                for field in EVALUATION_COUNT_FIELDS + EVALUATION_BOOLEAN_FIELDS:
                    i[field].append(d[field])
                i["escalate_wait"].append(d["escalate_wait"])
                p = {}
                total = d["performance"].get("total", None)
                if total:
                    for key, value in d["performance"].items():
                        p[key] = value / total * 100
                i["performance"].append(p)

                for field in ["upl", "subjects"]:
                    u = Counter(d[field])
                    total = len(d[field])
                    if total == 0:
                        total: None
                    if total:
                        u["total"] = total
                        for key, value in u.items():
                            u[key] = value / total * 100
                        u = dict(
                            sorted(u.items(), reverse=True, key=lambda item: item[1])
                        )
                    i[field].append(u)

            templist.append(i)
            labels = []
            for xval in xvalues:
                lbl = ""
                for idx, x in enumerate(xval):
                    lbl = lbl + str(x)
                    if idx < len(xval) - 1:
                        lbl = lbl + ","
                labels.append(lbl)

            for idx, x in enumerate(k):
                i[group_by_y[idx]] = x

            data["labels"] = labels
            data["datasets"] = templist
        return response.Response(data)
