from django.contrib import admin

from .models import MunicipalityConfiguration


@admin.register(MunicipalityConfiguration)
class MunicipalityConfigurationAdmin(admin.ModelAdmin):
    list_display = [
        "municipality_slug",
        "archiving_period",
    ]
    search_fields = [
        "municipality_slug",
    ]
    list_filter = [
        "municipality_slug",
    ]
