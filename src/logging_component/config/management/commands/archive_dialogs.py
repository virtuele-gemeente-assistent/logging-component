from django.core.management.base import BaseCommand
from django.db import transaction

from ...utils import archive_dialogs


class Command(BaseCommand):
    help = "Archive dialogs for all municipalities"

    @transaction.atomic
    def handle(self, *args, **options):
        archive_dialogs()
