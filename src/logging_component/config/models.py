from django.db import models
from django.utils.translation import gettext_lazy as _


class MunicipalityConfiguration(models.Model):
    municipality_slug = models.CharField(unique=True, max_length=512)
    archiving_period = models.PositiveSmallIntegerField(
        help_text=_("The number of days events will be retained in the database"),
        default=30,
    )
