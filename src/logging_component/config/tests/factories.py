from factory.django import DjangoModelFactory

from ..models import MunicipalityConfiguration


class MunicipalityConfigurationFactory(DjangoModelFactory):
    class Meta:
        model = MunicipalityConfiguration
