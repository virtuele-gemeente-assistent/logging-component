import logging
from datetime import timedelta

from django.utils import timezone

from logging_component.events.models import Event

from .models import MunicipalityConfiguration

logger = logging.getLogger(__name__)


def archive_dialogs_for_municipality(municipality_slug: str) -> None:
    logger.info("Archiving dialogs for %s", municipality_slug)

    try:
        config = MunicipalityConfiguration.objects.get(
            municipality_slug=municipality_slug
        )
        archiving_period = config.archiving_period
    except MunicipalityConfiguration.DoesNotExist:
        archiving_period = 30

    if archiving_period == 0:
        logger.info(
            "Archiving period set to 0 for %s, not archiving", municipality_slug
        )
        return

    events = Event.objects.filter(organisation_id=municipality_slug)
    threshold = timezone.now() - timedelta(days=archiving_period)

    events_to_delete = events.filter(timestamp__lte=threshold)
    logger.info("%s events to delete", events_to_delete.count())
    events_to_delete.delete()


def archive_dialogs() -> None:
    all_municipalities = Event.objects.values_list(
        "organisation_id", flat=True
    ).distinct()
    for municipality in all_municipalities:
        archive_dialogs_for_municipality(municipality)
