from django.contrib import admin

from .models import Event


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = [
        "event_id",
        "timestamp",
        "session_id",
        "agent_type",
        "event_type",
        "sender_agent_id",
        "recipient_agent_id",
        "channel",
        "organisation_id",
        "message",
    ]
    search_fields = [
        "event_id",
        "session_id",
        "message",
    ]
    list_filter = [
        "agent_type",
        "event_type",
        "organisation_id",
        "channel",
    ]
    readonly_fields = [field.name for field in Event._meta.fields]
