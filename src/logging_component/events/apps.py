from django.apps import AppConfig


class EventsConfig(AppConfig):
    name = "logging_component.events"
