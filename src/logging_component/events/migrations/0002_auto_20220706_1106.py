# Generated by Django 3.2.13 on 2022-07-06 09:06

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("events", "0001_initial"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="event",
            name="original_message",
        ),
        migrations.AddField(
            model_name="event",
            name="buttons",
            field=django.contrib.postgres.fields.ArrayField(
                base_field=models.CharField(max_length=512),
                blank=True,
                default=list,
                help_text="De inkomende knoppen (mogelijk vertaald).",
                size=None,
            ),
        ),
        migrations.AddField(
            model_name="event",
            name="incoming_data",
            field=models.JSONField(
                blank=True,
                default=dict,
                help_text="De originele data zoals de router het ontvangen heeft.",
            ),
        ),
        migrations.AddField(
            model_name="event",
            name="outgoing_data",
            field=models.JSONField(
                blank=True,
                default=dict,
                help_text="De originele data zoals de router het doorgestuurd heeft.",
            ),
        ),
    ]
