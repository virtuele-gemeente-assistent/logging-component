# Generated by Django 3.2.13 on 2022-07-21 07:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("events", "0003_event_translated_to"),
    ]

    operations = [
        migrations.AddField(
            model_name="event",
            name="test_session",
            field=models.BooleanField(
                default=False, help_text="Geeft aan of de sessie een testsessie is."
            ),
        ),
    ]
