# Generated by Django 3.2.15 on 2023-04-20 08:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("events", "0004_event_test_session"),
    ]

    operations = [
        migrations.RenameField(
            model_name="event",
            old_name="agent_id",
            new_name="sender_agent_id",
        ),
    ]
