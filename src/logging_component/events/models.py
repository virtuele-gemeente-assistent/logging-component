import uuid

from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils.translation import gettext_lazy as _


class Event(models.Model):
    event_id = models.UUIDField(
        unique=True,
        default=uuid.uuid4,
        help_text="Unieke resource identifier van het event (UUID4).",
    )
    session_id = models.CharField(
        unique=False,
        help_text="Unieke resource identifier van de sessie (UUID4).",
        max_length=256,
    )
    timestamp = models.DateTimeField(
        auto_now=True,
        help_text=_(
            "De datumtijd in ISO8601 formaat waarop dit event geregistreerd is."
        ),
        db_index=True,
    )
    event_type = models.CharField(
        max_length=256, help_text=_("Het type van dit event.")
    )
    agent_type = models.CharField(
        max_length=256,
        help_text=_("Het type agent waartoe dit event behoort."),
        blank=True,
    )
    sender_agent_id = models.CharField(
        max_length=256,
        help_text=_("De unieke identificatie van de uitzendende agent."),
        blank=True,
    )
    recipient_agent_id = models.CharField(
        max_length=256,
        help_text=_("De unieke identificatie van de ontvangende agent."),
        blank=True,
    )
    message = models.TextField(
        help_text=_("Het inkomende bericht (mogelijk vertaald)."), blank=True
    )
    buttons = ArrayField(
        models.CharField(max_length=512),
        help_text=_("De inkomende knoppen (mogelijk vertaald)."),
        blank=True,
        default=list,
    )

    # Separate model?
    incoming_data = models.JSONField(
        help_text=_("De originele data zoals de router het ontvangen heeft."),
        blank=True,
        default=dict,
    )
    outgoing_data = models.JSONField(
        help_text=_("De originele data zoals de router het doorgestuurd heeft."),
        blank=True,
        default=dict,
    )
    translated_from = models.CharField(
        max_length=2,
        help_text=_("De taal van het originele bericht (ISO 639-1)."),
        blank=True,
    )
    translated_to = models.CharField(
        max_length=2,
        help_text=_("De taal van het vertaalde bericht (ISO 639-1)."),
        blank=True,
    )
    translated_by = models.CharField(
        max_length=256,
        help_text=_("De plugin waarmee het bericht vertaald is."),
        blank=True,
    )
    organisation_id = models.CharField(
        max_length=256,
        help_text=_(
            "De unieke identificatie van de organisatie waarmee het gesprek gevoerd is."
        ),
        blank=True,
    )
    source = models.CharField(
        max_length=2048,
        help_text=_("De pagina waarop het gesprek gestart is."),
        blank=True,
    )
    channel = models.CharField(
        max_length=256,
        help_text=_("Het kanaal waarmee het gesprek gevoerd is."),
    )
    test_session = models.BooleanField(
        default=False,
        help_text=_("Geeft aan of de sessie een testsessie is."),
    )

    class Meta:
        verbose_name = "event"
        verbose_name_plural = "events"

    def __str__(self):
        return f"Event({str(self.event_id)[:6]} - {str(self.session_id)[:6]})"
