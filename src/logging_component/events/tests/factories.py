from typing import List
from uuid import uuid4

from factory.django import DjangoModelFactory

from ..models import Event


class EventFactory(DjangoModelFactory):
    class Meta:
        model = Event


def generate_basic_dialog(organisation_id: str) -> List[Event]:
    dialog_uuid = uuid4()
    event1 = EventFactory.create(
        session_id=dialog_uuid,
        event_type="agent_message",
        agent_type="bot",
        sender_agent_id="gem",
        message="Hallo, ik ben Gem, de chatbot van de gemeente",
        organisation_id=organisation_id,
    )
    event2 = EventFactory.create(
        session_id=dialog_uuid,
        event_type="user_message",
        message="Wat kost een paspoort?",
        organisation_id=organisation_id,
    )
    event3 = EventFactory.create(
        session_id=dialog_uuid,
        event_type="agent_message",
        agent_type="bot",
        sender_agent_id="gem",
        message="Een paspoort kost 50 euro",
        organisation_id=organisation_id,
    )
    return event1, event2, event3
