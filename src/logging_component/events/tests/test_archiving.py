from django.test import TestCase

from freezegun import freeze_time

from logging_component.config.tests.factories import MunicipalityConfigurationFactory
from logging_component.config.utils import archive_dialogs

from ..models import Event
from .factories import generate_basic_dialog


class ArchivingTestCase(TestCase):
    def setUp(self):
        super().setUp()

        with freeze_time("2022-01-01T12:00:00"):
            self.dialog1 = generate_basic_dialog("Roosendaal")

        with freeze_time("2022-02-01T12:00:00"):
            self.dialog2 = generate_basic_dialog("Roosendaal")

        with freeze_time("2022-01-01T12:00:00"):
            self.dialog3 = generate_basic_dialog("Utrecht")

        with freeze_time("2022-01-01T12:00:00"):
            self.dialog4 = generate_basic_dialog("Rotterdam")

        with freeze_time("2020-02-01T12:00:00"):
            self.dialog5 = generate_basic_dialog("Heusden")

        MunicipalityConfigurationFactory.create(
            municipality_slug="Roosendaal", archiving_period=30
        )
        MunicipalityConfigurationFactory.create(
            municipality_slug="Utrecht", archiving_period=60
        )
        MunicipalityConfigurationFactory.create(
            municipality_slug="Heusden", archiving_period=0
        )

    @freeze_time("2022-02-15T12:00:00")
    def test_archive_dialogs(self):
        archive_dialogs()

        roosendaal_events = Event.objects.filter(organisation_id="Roosendaal")

        self.assertEqual(roosendaal_events.count(), 3)

        # First dialog is removed
        for event in self.dialog1:
            with self.assertRaises(Event.DoesNotExist):
                event.refresh_from_db()

        # Second dialog remains
        for event in self.dialog2:
            event.refresh_from_db()

        utrecht_events = Event.objects.filter(organisation_id="Utrecht")

        self.assertEqual(utrecht_events.count(), 3)

        # Dialog remains
        for event in self.dialog3:
            event.refresh_from_db()

        rotterdam_events = Event.objects.filter(organisation_id="Rotterdam")

        self.assertEqual(rotterdam_events.count(), 0)

        # Dialog removed
        for event in self.dialog4:
            with self.assertRaises(Event.DoesNotExist):
                event.refresh_from_db()

        heusden_events = Event.objects.filter(organisation_id="Heusden")

        # Not removed, because archiving period was set to 0
        self.assertEqual(heusden_events.count(), 3)
