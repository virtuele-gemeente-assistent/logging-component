from django.contrib import admin

from .models import ConversationStatistics


@admin.register(ConversationStatistics)
class ConversationStatisticsAdmin(admin.ModelAdmin):
    list_display = [
        "session_id",
        "start",
    ]
    search_fields = [
        "session_id",
    ]
    list_filter = [
        "escalate",
        "escalate_result",
        "feedback",
        "feedback_result",
        "is_evaluated",
    ]
