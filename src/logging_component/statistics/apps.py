from django.apps import AppConfig


class StatisticsConfig(AppConfig):
    name = "logging_component.statistics"
