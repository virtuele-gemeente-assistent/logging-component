import logging
from collections import Counter, defaultdict

from django.core.management import BaseCommand

from logging_component.events.models import Event
from logging_component.statistics.models import ConversationStatistics
from logging_component.statistics.utils import (
    extract_metadata,
    import_dialog_evaluations,
    set_statistics,
)

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Generates statistics for logged conversations"

    def add_arguments(self, parser):
        parser.add_argument(
            "--use-test-sessions",
            action="store_true",
            help="Your boolean argument help message",
        )
        parser.add_argument(
            "--delete-old",
            action="store_true",
            help="Your boolean argument help message",
        )

    def handle(self, *args, **options):
        use_test_sessions = options["use_test_sessions"]
        delete_old = options["delete_old"]

        if delete_old:
            ConversationStatistics.objects.all().delete()

        logged_sessions = ConversationStatistics.objects.values_list(
            "session_id", flat=True
        )
        logger.info(
            "Generating statistics from conversations, test_sessions: %s",
            use_test_sessions,
        )
        events = (
            Event.objects.exclude(session_id__in=logged_sessions)
            .filter(test_session=use_test_sessions)
            .order_by("timestamp")
        )
        mapping = {}
        last_events = defaultdict(dict)

        for event in events:
            if event.session_id not in mapping:
                # TODO only log if 12 hours since last message?
                # TODO what if same attribute occurs twice?
                # TODO duplicate upl names? is there a max?
                cal = event.timestamp.isocalendar()
                mapping[event.session_id] = ConversationStatistics(
                    session_id=event.session_id,
                    organisation_id=event.organisation_id,
                    start=event.timestamp,
                    end=event.timestamp,
                    year=event.timestamp.year,
                    iso_weekdate_year=cal.year,
                    month=event.timestamp.month,
                    week=cal.week,
                    weekday=event.timestamp.isoweekday(),
                    date=event.timestamp.date(),
                    hour=event.timestamp.hour,  # daylight savings
                    performance=Counter(),
                )

            saved_statistics = mapping[event.session_id]

            if "customData" in event.incoming_data:
                extract_metadata(
                    saved_statistics,
                    event.incoming_data["customData"],
                    "source",
                    "chatStartedOnPage",
                )
                last_events[event.session_id]["event"] = "user"

            if "metadata" in event.incoming_data:
                event_metadata = event.incoming_data["metadata"]
                if event_metadata.get("statistics"):
                    statistics = event_metadata["statistics"]
                    set_statistics(
                        saved_statistics,
                        statistics,
                        event.timestamp,
                        last_events,
                        event.session_id,
                    )

                    last_events[event.session_id]["event"] = "bot"

            if event.timestamp > saved_statistics.end:
                saved_statistics.end = event.timestamp

            # Store the changed result
            mapping[event.session_id] = saved_statistics

        to_be_created = [
            conversation_statistic
            for _, conversation_statistic in mapping.items()
            if conversation_statistic.performance.get("total", 0) > 0
        ]
        ConversationStatistics.objects.bulk_create(to_be_created)

        logger.info("Statistics conversion done")

        mapping = {
            statistics.session_id: statistics
            for statistics in ConversationStatistics.objects.filter(
                is_evaluated=False
            ).iterator()
        }
        import_dialog_evaluations(mapping)
