import logging
from collections import Counter, defaultdict
from copy import deepcopy
from datetime import datetime

from django.core.management import BaseCommand
from django.db import transaction
from django.db.models.expressions import RawSQL
from django.utils.timezone import make_aware

import pytz
import requests
import yaml

from logging_component.statistics.models import ConversationStatistics, EventbrokerEvent
from logging_component.statistics.utils import (
    extract_metadata,
    import_dialog_evaluations,
    set_statistics,
)

logger = logging.getLogger(__name__)

RESPONSES_URL = "https://gitlab.com/virtuele-gemeente-assistent/gem/-/raw/master/rasa/domain/responses.yml"


class Command(BaseCommand):
    help = "Generates statistics for historic eventbroker data"

    def add_arguments(self, parser):
        parser.add_argument(
            "--use-test-sessions",
            action="store_true",
            help="Indicates whether or not test sessions should be used for statistics",
        )
        parser.add_argument(
            "--delete-old",
            action="store_true",
            help="Indicates whether or not old statistics should be deleted",
        )
        parser.add_argument(
            "--start-date",
            type=str,
            nargs="?",
            default="",
            help="Parse events for specific municipality",
        )
        parser.add_argument(
            "--end-date",
            type=str,
            nargs="?",
            default="",
            help="Parse events for specific municipality",
        )

    @transaction.atomic
    def handle(self, *args, **options):
        use_test_sessions = options["use_test_sessions"]
        delete_old = options["delete_old"]
        start_date = options["start_date"]
        end_date = options["end_date"]

        if delete_old:
            ConversationStatistics.objects.all().delete()

        response = requests.get(RESPONSES_URL)
        responses = yaml.safe_load(response.content)["responses"]

        for key, values in responses.items():
            responses[key] = None
            for i in values:
                if i.get("custom") and i["custom"].get("statistics"):
                    responses[key] = i["custom"]["statistics"]
                    break

        logged_sessions = set(
            ConversationStatistics.objects.values_list("session_id", flat=True)
        )
        annotated = EventbrokerEvent.objects.using("eventbroker").annotate(
            timestamp=RawSQL("(events.data::json->>'timestamp')::double precision", ()),
        )

        if start_date and end_date:
            start_date = make_aware(datetime.fromisoformat(start_date), is_dst=True)
            end_date = make_aware(datetime.fromisoformat(end_date), is_dst=True)
            annotated = annotated.filter(
                timestamp__gte=start_date.timestamp(),
                timestamp__lte=end_date.timestamp(),
            )

        events = annotated.order_by("id")

        logger.info(
            "Generating statistics from eventbroker data, test_sessions: %s",
            use_test_sessions,
        )
        ignore_ids = set()

        mapping = {}
        last_events = defaultdict(dict)

        iteration = 0
        for event in events.iterator():
            if iteration % 100000 == 0:
                logger.info("Reached event #%s", iteration)
            iteration += 1

            sender_id = event.sender_id
            if sender_id in logged_sessions:
                continue

            event_type = event.data["event"]
            if sender_id in ignore_ids or event_type not in [
                "user",
                "bot",
            ]:
                continue

            if not use_test_sessions and event.data.get("metadata", {}).get("username"):
                if sender_id in mapping:
                    del mapping[sender_id]
                ignore_ids.add(sender_id)
                continue

            timestamp = make_aware(
                datetime.fromtimestamp(event.data["timestamp"]),
                timezone=pytz.timezone("Europe/Amsterdam"),
                is_dst=True,
            )

            if sender_id not in mapping:
                # TODO only log if 12 hours since last message?
                # TODO what if same attribute occurs twice?
                # TODO duplicate upl names? is there a max?
                cal = timestamp.isocalendar()
                mapping[sender_id] = ConversationStatistics(
                    session_id=sender_id,
                    # organisation_id=event.organisation_id,
                    start=timestamp,
                    end=timestamp,
                    year=timestamp.year,
                    iso_weekdate_year=cal.year,
                    month=timestamp.month,
                    week=cal.week,
                    weekday=timestamp.isoweekday(),
                    date=timestamp.date(),
                    hour=timestamp.hour,  # daylight savings?
                    performance=Counter(),
                )

            saved_statistics = mapping[sender_id]
            event_metadata = event.data["metadata"]

            if event_type == "user":
                extract_metadata(
                    saved_statistics, event_metadata, "source", "chatStartedOnPage"
                )
                extract_metadata(
                    saved_statistics, event_metadata, "organisation_id", "municipality"
                )
                last_events[sender_id]["event"] = "user"
            elif event_type == "bot":
                statistics = {}
                if "end2end" not in event_metadata:
                    continue

                template_name = event_metadata["end2end"]["template_name"]
                template_vars = event_metadata["end2end"]["template_vars"]
                if responses.get(template_name):
                    # Retrieve statistics for utter from responses.yml
                    statistics = deepcopy(responses[template_name])
                    for key, value in statistics.items():
                        if key == "upl" and value == "{product}":
                            statistics["upl"] = template_vars.get("product", None)

                    set_statistics(
                        saved_statistics, statistics, timestamp, last_events, sender_id
                    )

                    # Workaround for historic data
                    last_event = last_events[sender_id]
                    if (
                        template_name == "utter_lo_gemeente_contact_lokaal_livechat"
                        and last_event.get("escalate")
                        and last_event.get("event") == "bot"
                    ):
                        saved_statistics.escalate_result = "no_livechat"
                        saved_statistics.escalate_result_time = timestamp

                    if statistics.get("escalate", False):
                        last_events[sender_id]["escalate"] = True
                    else:
                        last_events[sender_id]["escalate"] = False

                    last_events[sender_id]["event"] = "bot"

            if timestamp > saved_statistics.end:
                saved_statistics.end = timestamp

            # Store the changed result
            mapping[sender_id] = saved_statistics

        to_be_created = []
        for _, conversation_statistic in mapping.items():
            if conversation_statistic.performance.get("total", 0) <= 0:
                continue

            if conversation_statistic.start < make_aware(
                datetime(2023, 4, 7), is_dst=True
            ):
                if conversation_statistic.escalate == "direct":
                    continue

                if conversation_statistic.escalate_result == "closed":
                    conversation_statistic.escalate_result = "not_available"

                conversation_statistic.escalate_result_time = None
                conversation_statistic.escalate_time = None
                conversation_statistic.escalate_wait = None

            to_be_created.append(conversation_statistic)

        ConversationStatistics.objects.bulk_create(to_be_created)

        logger.info("Statistics conversion done")

        mapping = {
            statistics.session_id: statistics
            for statistics in ConversationStatistics.objects.filter(
                is_evaluated=False
            ).iterator()
        }

        import_dialog_evaluations(mapping)
