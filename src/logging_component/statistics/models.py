from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils.translation import gettext_lazy as _


class ConversationStatistics(models.Model):
    session_id = models.CharField(
        unique=True,
        help_text="Unieke resource identifier van de sessie (UUID4).",
        max_length=256,
    )
    organisation_id = models.CharField(
        max_length=256,
        help_text=_(
            "De unieke identificatie van de organisatie waarmee het gesprek gevoerd is."
        ),
        blank=True,
    )
    start = models.DateTimeField(
        help_text=_("De datumtijd in ISO8601 formaat waarop dit gesprek begonnen is."),
        db_index=True,
    )
    end = models.DateTimeField(
        help_text=_("De datumtijd in ISO8601 formaat waarop dit gesprek gestopt is."),
        db_index=True,
    )
    year = models.IntegerField(
        help_text=_("Het jaar waarin het gesprek gevoerd is."),
        db_index=True,
    )
    iso_weekdate_year = models.IntegerField(
        help_text=_(
            "De ISO week date jaar waarin het gesprek gevoerd is (https://en.wikipedia.org/wiki/ISO_week_date)."
        ),
        db_index=True,
    )
    month = models.IntegerField(
        help_text=_("De maand waarin het gesprek gevoerd is."),
        db_index=True,
    )
    week = models.IntegerField(
        help_text=_("De week waarin het gesprek gevoerd is."),
        db_index=True,
    )
    weekday = models.IntegerField(
        help_text=_("De weekdag waarop het gesprek gevoerd is."),
        db_index=True,
    )
    hour = models.IntegerField(
        help_text=_("De weekdag waarop het gesprek gevoerd is."),
        db_index=True,
    )
    date = models.DateField(
        help_text=_("De datum waarop het gesprek gevoerd is."),
        db_index=True,
    )
    source = models.CharField(
        max_length=2048,
        help_text=_("De pagina waarop het gesprek gestart is."),
        blank=True,
    )
    escalate = models.CharField(
        max_length=128,
        help_text=_("Geeft aan of er escalatie plaatsgevonden."),
        blank=True,
    )
    escalate_result = models.CharField(
        max_length=128,
        help_text=_("Geeft het resultaat van escalatie aan."),
        blank=True,
    )
    feedback = models.CharField(
        max_length=128,
        help_text=_("Geeft aan of er feedback is gevraagd/gegeven."),
        blank=True,
    )
    feedback_result = models.CharField(
        max_length=128,
        help_text=_("Geeft het resultaat van de feedback aan."),
        blank=True,
    )
    upl = ArrayField(
        models.CharField(max_length=512),
        help_text=_("De onderwerpen van het gesprek."),
        blank=True,
        default=list,
    )
    performance = models.JSONField(
        help_text=_("Geeft de performance van de bot in het gesprek aan."),
        blank=True,
        default=dict,
    )
    escalate_time = models.DateTimeField(
        help_text=_("De datumtijd in ISO8601 formaat waarop escalatie begonnen is."),
        null=True,
        blank=True,
    )
    escalate_result_time = models.DateTimeField(
        help_text=_(
            "De datumtijd in ISO8601 formaat waarop de escalatie gestopt/voltooid is."
        ),
        null=True,
        blank=True,
    )
    escalate_wait = models.PositiveIntegerField(
        help_text=_(
            "De wachttijd in seconden tussen het moment van escalatie en einde escalatie."
        ),
        blank=True,
        null=True,
    )
    is_evaluated = models.BooleanField(
        help_text=_("Geeft aan of er voor dit gesprek turfdata is"),
        default=False,
    )

    # DialogEvaluation fields:
    subjects = ArrayField(
        models.CharField(
            verbose_name=_("Onderwerp(en) van het gesprek"), max_length=256
        ),
        verbose_name=_("Onderwerp(en) van het gesprek"),
        help_text=_("Onderwerp(en) van het gesprek"),
        default=list,
        blank=True,
    )
    num_correct_answers = models.PositiveSmallIntegerField(
        verbose_name=_("Hoe vaak gaf Gem direct juiste antwoord?"),
        null=True,
        blank=True,
    )
    num_correct_answers_after_one_correction = models.PositiveSmallIntegerField(
        verbose_name=_("Hoe vaak gaf Gem een correct antwoord na 1 reparatie?"),
        null=True,
        blank=True,
    )
    num_correct_answers_after_two_corrections = models.PositiveSmallIntegerField(
        verbose_name=_("Hoe vaak gaf Gem een correct antwoord na 2 reparaties?"),
        null=True,
        blank=True,
    )
    num_failed_corrections = models.PositiveSmallIntegerField(
        verbose_name=_("Hoe vaak kon Gem niet goed repareren?"),
        null=True,
        blank=True,
    )
    num_incorrect_responses = models.PositiveSmallIntegerField(
        verbose_name=_("Hoe vaak gaf Gem een verkeerd antwoord (plank mis)?"),
        null=True,
        blank=True,
    )
    num_impossible_questions = models.PositiveSmallIntegerField(
        verbose_name=_("Hoe vaak stelde de inwoner een onmogelijke vraag?"),
        null=True,
        blank=True,
    )
    citizen_properly_assisted = models.BooleanField(
        verbose_name=_("Heeft de inwoner correcte dienstverlening ontvangen?"),
        null=True,
    )
    critical_error = models.BooleanField(
        verbose_name=_("Kritische fout?"), default=False
    )
    different_language = models.BooleanField(
        verbose_name=_("Andere taal?"),
        default=False,
    )
    personal_question = models.BooleanField(
        verbose_name=_("Persoonlijke vraag?"),
        default=False,
    )
    classification = models.CharField(
        max_length=128,
        help_text=_("Kwaliteit van de assistent"),
        null=True,
        blank=True,
    )
    remarks = models.TextField(
        verbose_name=_("Opmerkingen"),
        blank=True,
    )

    class Meta:
        verbose_name = "conversation statistics"
        verbose_name_plural = "conversation statistics"

    def __str__(self):
        return f"ConversationStatistics({str(self.session_id)[:6]})"


class EventbrokerEvent(models.Model):
    """
    Separate temporary model to query the eventbroker table
    """

    id = models.IntegerField(primary_key=True)
    sender_id = models.CharField(max_length=255)
    data = models.JSONField()

    class Meta:
        managed = False
        db_table = "events"


# Duplicated from CMS
class DialogEvaluation(models.Model):
    session_id = models.CharField(
        verbose_name=_("Sessie ID"),
        max_length=256,
        help_text=_("ID van het gesprek in het logging component/CMS."),
    )
    chat_started_on_page = models.URLField(
        verbose_name=_("Gesprek gestart op pagina"),
        max_length=2000,
        help_text=_("URL van de webpagina waarop het gesprek gestart is."),
        blank=True,
    )
    subjects = ArrayField(
        models.CharField(
            verbose_name=_("Onderwerp(en) van het gesprek"), max_length=256
        ),
        verbose_name=_("Onderwerp(en) van het gesprek"),
        help_text=_("Onderwerp(en) van het gesprek"),
        default=list,
        blank=True,
    )

    # Question recognition
    num_correct_answers = models.PositiveSmallIntegerField(
        verbose_name=_("Hoe vaak gaf Gem direct juiste antwoord?"),
        null=True,
        blank=True,
    )
    num_correct_answers_after_one_correction = models.PositiveSmallIntegerField(
        verbose_name=_("Hoe vaak gaf Gem een correct antwoord na 1 reparatie?"),
        null=True,
        blank=True,
    )
    num_correct_answers_after_two_corrections = models.PositiveSmallIntegerField(
        verbose_name=_("Hoe vaak gaf Gem een correct antwoord na 2 reparaties?"),
        null=True,
        blank=True,
    )
    num_failed_corrections = models.PositiveSmallIntegerField(
        verbose_name=_("Hoe vaak kon Gem niet goed repareren?"),
        null=True,
        blank=True,
    )
    num_incorrect_responses = models.PositiveSmallIntegerField(
        verbose_name=_("Hoe vaak gaf Gem een verkeerd antwoord (plank mis)?"),
        null=True,
        blank=True,
    )
    num_impossible_questions = models.PositiveSmallIntegerField(
        verbose_name=_("Hoe vaak stelde de inwoner een onmogelijke vraag?"),
        null=True,
        blank=True,
    )

    # KPI's
    citizen_properly_assisted = models.BooleanField(
        verbose_name=_("Heeft de inwoner correcte dienstverlening ontvangen?"),
        null=True,
    )
    critical_error = models.BooleanField(
        verbose_name=_("Kritische fout?"), default=False
    )
    escalation_to_livechat = models.BooleanField(
        verbose_name=_("Doorverwezen naar KCC/livechat?"), default=False
    )
    detected_escalation_to_livechat = models.CharField(
        verbose_name=_("Livechat status (automatisch afgeleid)"),
        blank=True,
        max_length=100,
    )
    livechat_reason = ArrayField(
        models.CharField(
            max_length=100, verbose_name=_("Doorverwijzing naar KCC/livechat na")
        ),
        verbose_name=_("Doorverwijzing naar KCC/livechat na"),
        default=list,
        blank=True,
    )
    successful_livechat_escalation = models.CharField(
        verbose_name=_("Doorschakeling naar livechat gelukt?"),
        max_length=100,
        blank=True,
    )
    failed_escalation_reason = models.CharField(
        verbose_name=_("Reden livechat niet gelukt"),
        max_length=100,
        blank=True,
    )

    # Other
    different_language = models.BooleanField(
        verbose_name=_("Andere taal?"),
        default=False,
    )
    personal_question = models.BooleanField(
        verbose_name=_("Persoonlijke vraag?"),
        default=False,
    )
    remarks = models.TextField(
        verbose_name=_("Opmerkingen"),
        blank=True,
    )

    class Meta:
        managed = False
        db_table = "dialogs_dialogevaluation"
