import logging

from .models import DialogEvaluation

logger = logging.getLogger(__name__)

EVALUATION_FIELDS = [
    "subjects",
    "num_correct_answers",
    "num_correct_answers_after_one_correction",
    "num_correct_answers_after_two_corrections",
    "num_failed_corrections",
    "num_incorrect_responses",
    "num_impossible_questions",
    "citizen_properly_assisted",
    "critical_error",
    "different_language",
    "personal_question",
    "remarks",
]


def extract_metadata(
    saved_statistics, event_metadata, statistics_attr_name, metadata_key
):
    if metadata_key in event_metadata and not getattr(
        saved_statistics, statistics_attr_name, None
    ):
        setattr(saved_statistics, statistics_attr_name, event_metadata[metadata_key])


def set_statistics(
    saved_statistics, event_statistics, event_timestamp, last_events, sender_id
):
    for field in [
        "escalate",
        "escalate_result",
        "feedback",
        "feedback_result",
    ]:
        value = event_statistics.get(field, "")
        if value:
            if field == "escalate":
                if not saved_statistics.escalate_result:
                    saved_statistics.escalate_result = "unknown"

                saved_statistics.escalate_time = event_timestamp
            elif field == "escalate_result":
                if not saved_statistics.escalate:
                    saved_statistics.escalate = "unknown"

                saved_statistics.escalate_result_time = event_timestamp

            setattr(saved_statistics, field, value)

    if event_statistics.get("upl") and not last_events[sender_id].get("upl"):
        saved_statistics.upl.append(event_statistics.get("upl"))
        last_events[sender_id]["upl"] = True
    else:
        last_events[sender_id]["upl"] = False

    # Ignore performance for subsequent bot responses
    if (
        event_statistics.get("performance")
        and not last_events[sender_id].get("event") == "bot"
    ):
        value = event_statistics["performance"]
        # For historical data
        if value == "out_of_scope":
            value = "unsupported"
        saved_statistics.performance[value] += 1
        saved_statistics.performance["total"] += 1

    if saved_statistics.escalate_time and saved_statistics.escalate_result_time:
        saved_statistics.escalate_wait = (
            saved_statistics.escalate_result_time - saved_statistics.escalate_time
        ).total_seconds()

        if saved_statistics.escalate_wait < 0:
            saved_statistics.escalate_wait = None
            saved_statistics.escalate_time = None
            saved_statistics.escalate_result_time = None


def import_dialog_evaluations(mapping):
    evaluations = DialogEvaluation.objects.using("antwoorden_cms").filter(
        session_id__in=mapping.keys()
    )
    i = 0
    for evaluation in evaluations.iterator():
        if i % 1000 == 0:
            logger.info("Reached evaluation #%d", i)
        i += 1

        statistics = mapping[evaluation.session_id]
        for field in EVALUATION_FIELDS:
            setattr(statistics, field, getattr(evaluation, field))

        statistics.is_evaluated = True
        statistics.save()

    logging.info("Import of dialog evaluations successful")
