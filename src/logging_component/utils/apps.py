from django.apps import AppConfig


class UtilsConfig(AppConfig):
    name = "logging_component.utils"

    def ready(self):
        from . import checks  # noqa
