"""
WSGI config for logging_component project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/wsgi/
"""
import os  # noqa flake8:skip

from django.core.wsgi import get_wsgi_application

from logging_component.setup import setup_env

setup_env()

application = get_wsgi_application()
